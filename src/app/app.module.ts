import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {NgbDropdownModule, NgbModule, NgbAccordionModule} from "@ng-bootstrap/ng-bootstrap";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { MenuComponent } from './menu/menu.component';
import { MenuSmallComponent } from './menu-small/menu-small.component';
import { ScrollPositionDirective } from './directive/scroll-position.directive';
import { AccueilComponent } from './accueil/accueil.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    MenuSmallComponent,
    ScrollPositionDirective,
    AccueilComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,NgbModule, NgbDropdownModule, NgbAccordionModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
