import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { FactoryService } from '../service/factory.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  //encapsulation: ViewEncapsulation.None
})
export class MenuComponent implements OnInit {
  posY = 0;
  posYSubscription: Subscription = new Subscription;
  constructor(private factoryService: FactoryService) { }

  ngOnInit(): void {
    this.posYSubscription = this.factoryService.scrollEleObject.subscribe(
      (pos) => {
        //console.log(this.posY);
        this.posY = pos;
      }
    );
  }

}
