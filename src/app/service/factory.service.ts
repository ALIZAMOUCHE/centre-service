import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FactoryService {
  scrollEleObject = new Subject<number>();

  constructor() { }

  setScrollEleObject(pos: number){
    this.scrollEleObject.next(pos);
  }
}
