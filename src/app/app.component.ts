import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FactoryService } from './service/factory.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'CENTRE-SERVICE';

  posY = 0;
  posYSubscription: Subscription = new Subscription;
  constructor(private router: Router, private factoryService: FactoryService) { 
    this.posYSubscription = this.factoryService.scrollEleObject.subscribe(
      (pos) => {
        console.log(this.posY);
        this.posY = pos;
      }
    );

    //this.router.navigate(['accueil']);
  }
  onSave() {
    console.log('fddddddddddddddddddddddddddd');
  }
  scrollTop() {
    window.scrollTo(0, 0)
  }
}
