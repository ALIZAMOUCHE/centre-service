import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FactoryService } from '../service/factory.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  StrategicLocations =0;
  DedicatedDevelopers =0;
  CustomApps =0;
  SpecializedIndustries=0;
  GlobalCustomers=0;
  DevelopmentAwards=0;
  posY = 0;
  posYSubscription: Subscription = new Subscription;

  constructor(private factoryService: FactoryService) {  }

  ngOnInit(): void {
    this.posYSubscription = this.factoryService.scrollEleObject.subscribe(
      (pos) => {
        //console.log(this.posY);
        this.posY = pos;
        if (this.posY === 2185 && this.StrategicLocations === 0){
          for (let index = 0; index < 16; index++) {this.StrategicLocations++; }
          for (let index = 0; index < 2600; index++) {this.DedicatedDevelopers++; }
          for (let index = 0; index < 16000; index++) {this.CustomApps++; }
          for (let index = 0; index < 46; index++) {this.SpecializedIndustries++; }
          for (let index = 0; index < 3000; index++) {this.GlobalCustomers++; }
          for (let index = 0; index < 150; index++) {this.DevelopmentAwards++; }
        }
      }
    );
  }



}
