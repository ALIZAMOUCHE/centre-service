import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbDropdownModule, NgbModule} from "@ng-bootstrap/ng-bootstrap";

import { MaterialModule } from '../material/material.module';

import { SecteursRoutingModule } from './secteurs-routing.module';
import { SecteursComponent } from './secteurs.component';
import { BanqueComponent } from './banque/banque.component';
import { AgriculteurComponent } from './agriculteur/agriculteur.component';


@NgModule({
  declarations: [
    SecteursComponent,
    BanqueComponent,
    AgriculteurComponent
  ],
  imports: [
    CommonModule,
    SecteursRoutingModule,
    NgbDropdownModule,
    MaterialModule
  ]
})
export class SecteursModule { }
