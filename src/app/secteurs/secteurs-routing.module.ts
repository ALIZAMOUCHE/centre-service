import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from '../menu/menu.component';

import { SecteursComponent } from './secteurs.component';
import { BanqueComponent } from './banque/banque.component';
import { AgriculteurComponent } from './agriculteur/agriculteur.component';

const routes: Routes = [{ path: '', component: SecteursComponent, children: [
  { path: 'Banque', component: BanqueComponent },
  { path: 'Agriculteur', component: AgriculteurComponent }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecteursRoutingModule { }
