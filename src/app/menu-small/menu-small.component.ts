import { Component, OnInit } from '@angular/core';
import { NgbPanelChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { FactoryService } from '../service/factory.service';

@Component({
  selector: 'app-menu-small',
  templateUrl: './menu-small.component.html',
  styleUrls: ['./menu-small.component.scss']
})
export class MenuSmallComponent implements OnInit {
  isMenu = false;
  isSubMenu = false;
  isSubMenuSECTEURS = false;
  isSubMenuSOLUTIONS = false;
  isSubMenuSERVIES = false;
  isSubMenuPORTFOLIO = false;
  posY = 0;
  posYSubscription: Subscription = new Subscription;
  constructor(private factoryService: FactoryService) { }

  ngOnInit(): void {
    this.posYSubscription = this.factoryService.scrollEleObject.subscribe(
      (pos) => {
        //console.log(this.posY);
        this.posY = pos;
      }
    );
  }
  doSomething(){
    this.isMenu = !this.isMenu;
    this.factoryService.setScrollEleObject(100);
  }
  openSubMenu(x: any) {
    this.isSubMenu = !this.isSubMenu;
    if(x === 'SECTEURS') {
      this.isSubMenuSECTEURS = !this.isSubMenuSECTEURS;
      this.isSubMenuSOLUTIONS = false;
      this.isSubMenuSERVIES = false;
      this.isSubMenuPORTFOLIO = false;
    } else if(x === 'SOLUTIONS'){
      this.isSubMenuSOLUTIONS = !this.isSubMenuSOLUTIONS;
      this.isSubMenuSECTEURS = false;
      this.isSubMenuSERVIES = false;
      this.isSubMenuPORTFOLIO = false;
    } else if(x === 'SERVIES'){
      this.isSubMenuSERVIES = !this.isSubMenuSERVIES;
      this.isSubMenuSECTEURS = false;
      this.isSubMenuSOLUTIONS = false;
      this.isSubMenuPORTFOLIO = false;
    } else if(x === 'PORTFOLIO'){
      this.isSubMenuPORTFOLIO = !this.isSubMenuPORTFOLIO;
      this.isSubMenuSECTEURS = false;
      this.isSubMenuSOLUTIONS = false;
      this.isSubMenuSERVIES = false;
    }
  }

  
}
