import { Directive, HostListener } from '@angular/core';
import { FactoryService } from '../service/factory.service';

@Directive({
  selector: '[appScrollPosition]'
})
export class ScrollPositionDirective {

  constructor(private factoryService: FactoryService) { }

 /* @HostListener('window:scroll', ['$event.target'])
  onElementScroll(event: Event){
    console.log(event.scrollingElement.scrolltop);
  }*/

  @HostListener('window:scroll', [])
  onWindowScroll() {      
    this.factoryService.setScrollEleObject(window.scrollY);
  }
}
