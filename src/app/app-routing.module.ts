import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
const routes: Routes = [
  { path: 'accueil', component:  AccueilComponent},
  { path: 'secteurs', loadChildren: () => import('./secteurs/secteurs.module').then(m => m.SecteursModule) }, 
  { path: 'solutions', loadChildren: () => import('./solutions/solutions.module').then(m => m.SolutionsModule) }, 
  { path: 'services', loadChildren: () => import('./services/services.module').then(m => m.ServicesModule) }, 
  { path: 'portfolio', loadChildren: () => import('./portfolio/portfolio.module').then(m => m.PortfolioModule) },
  { path: '**', redirectTo: 'accueil' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
